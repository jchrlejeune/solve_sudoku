"""
Auteur: JCLejeune
Exercice FunMooc Python
"""
from copy import deepcopy
def zone(x,b,e):
    f=[]
    for i in range((e//3)*3,((e//3)*3)+3):
        for j in range((b//3)*3,((b//3)*3)+3):
            f.append(x[i][j])
    return f
def vertical(x,b):
    c=[]
    for i in range(9):
        c.append(x[i][b])
    return c   
def horizontal(x,a):
    d=[]
    for j in range(9):
        d.append(x[a][j])
    return d

def naked_single(a):
    oldgrid=a
    aa=deepcopy(a)
    cmptr=0
    newgrid=aa
    values=[]
    change=[]
    for i in range(0,len(aa)):       
        for j in range(0,len(aa[i])):
            if aa[i][j]==0:
                cmptr+=1
                values=list(set(horizontal(aa,i)+vertical(aa,j)+zone(aa,j,i)))
                values.remove(0)
                #print(values,'\t',len(values),'____ i: ', i,'j: ',j)
                if len(values)==8:
                    for x in range(1,10):
                        if x not in values:
                            #print('X: ',x)
                            change.append([i,j,x])

    #print(cmptr,' case(s) vide(s)\n',len(change), 'à remplir\n',change)
    
    for m in change:
        aa[m[0]][m[1]]=m[2]
        cmptr-=1
    newgrid=aa
    #print()
    if cmptr>0 and oldgrid!=newgrid:
        #for q in aa:
        #    print(q)
        #print('cmptr',cmptr,'=>> Continue')
        return naked_single(aa)
        
    elif cmptr==0 and newgrid!=oldgrid:
        #print(aa)
        #print('Grille Remplie, Tadaaaa')
        return(True,aa)
        
    else:
        #print('Stop!')
        return (False, None)
"""
#test
print(naked_single([[4, 0, 3, 0, 9, 6, 0, 1, 0],
              [0, 0, 2, 8, 0, 1, 0, 0, 3],
              [0, 1, 0, 0, 0, 0, 0, 0, 7],
              [0, 4, 0, 7, 0, 0, 0, 2, 6],
              [5, 0, 7, 0, 1, 0, 4, 0, 9],
              [1, 2, 0, 0, 0, 3, 0, 8, 0],
              [2, 0, 0, 0, 0, 0, 0, 7, 0],
              [7, 0, 0, 2, 0, 9, 8, 0, 0],
              [0, 6, 0, 1, 5, 0, 3, 0, 2]]))
"""
